DROP DATABASE IF EXISTS EQUITEL_ENGINES;
DROP TABLE IF EXISTS engines;
DROP TABLE IF EXISTS engine_parts;

CREATE DATABASE EQUITEL_ENGINES;


CREATE TABLE engines(
    engines_id SERIAL PRIMARY KEY,
    description VARCHAR(1000) NOT NULL,
    power VARCHAR(100),
    import_value float(8)
);


CREATE TABLE engine_parts(
    part_id SERIAL PRIMARY KEY,
    name VARCHAR(200) NOT NULL    
);

CREATE TABLE engine_specifications(
    id SERIAL PRIMARY key,
    engines_id INT,
    part_id INT,
    description VARCHAR(1000) NOT null,
   	FOREIGN KEY(engines_id) references  engines(engines_id),
    FOREIGN KEY(part_id) references  engine_parts(part_id)
);

INSERT INTO engines(description, power, import_value) VALUES ('engine M1', '4600HP', 15789.47);
INSERT INTO engine_parts(name) VALUES ('block'), ('piston'), ('valve'), ('cylinder head'), ('alternator'), ('radiator'), ('battery');
