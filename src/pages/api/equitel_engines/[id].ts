import { NextApiRequest, NextApiResponse } from "next";
// import { conn } from "../../../utils/database";
import  { conn } from  "src/utils/database";

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, query, body } = req;

  switch (method) {
    case "GET":
      try {
        const text = "SELECT * FROM engines WHERE engines_id = $1";
        const values = [query.id];
        const result = await conn.query(text, values);

        if (result.rows.length === 0) {
          return res.status(404).json({ message: "model not found" });
        }

        return res.json(result.rows[0]);
      } catch (error: any) {
        return res.status(500).json({ message: error.message });
      }

    case "DELETE":
      try {
        const text = "DELETE FROM engines WHERE engines_id = $1 RETURNING *";
        const values = [query.id];
        const result = await conn.query(text, values);
        console.log(result);

        if (result.rowsCount === 0) {
          return res.status(404).json({ message: "model not found" });
        }
        return res.json(result.rows[0]);
      } catch (error: any) {
        return res.status(500).json({ message: error.message });
      }

    case "PUT":
      const { description, power, import_value } = body;
      try {
        const text =
          "UPDATE engines SET description=$1, power = $2,  import_value = $3 WHERE engines_id = $4 RETURNING *";
        const values = [description, power, import_value, query.id];
        const result = await conn.query(text, values);
        console.log(result);

        if (result.rows.length === 0) {
          return res.status(404).json({ message: "model not found" });
        }
        return res.json(result.rows[0]);
      } catch (error: any) {
        return res.status(500).json({ message: error.message });
      }

    default:
      return res.status(400).json("method not allowed");
  }
};
