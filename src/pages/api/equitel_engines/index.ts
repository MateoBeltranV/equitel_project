import { NextApiRequest, NextApiResponse } from "next";
// import { conn } from "../../../utils/database";
import  { conn } from  "src/utils/database";

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, body } = req;

  switch (method) {
    case "GET":
      try {
        const query = "SELECT * FROM engines";
        const response = await conn.query(query);
        console.log(response);

        return res.status(200).json(response.rows);
      } catch (error) {
        console.log(error);
      }

    case "POST":
      try {
        const { description, power, import_value } = body;
        const query =
          "INSERT INTO engines(description, power, import_value) VALUES ($1, $2, $3)";
        const values = [description, power, import_value];
        const response = await conn.query(query, values);

        return res.status(200).json("creating status");
      } catch (error) {
        console.log(error);
      }

    default:
      return res.status(400).json("invalid method");
  }
};


// {
//     "description":"engine M3",
//     "power":"3000HP",
//     "import_value": 20000.4
// }