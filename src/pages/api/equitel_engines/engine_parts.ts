import { NextApiRequest, NextApiResponse } from "next";
import { conn } from '../../../utils/database';

// eslint-disable-next-line import/no-anonymous-default-export
export default async(req: NextApiRequest, res: NextApiResponse) =>  {
    const { method, body } = req;

    switch(method){
        case 'GET':
            return res.status(200).json('getting status');  
        case 'POST':
            const { name } = body;
            const query = 'INSERT INTO engine_parts(name) VALUES ($1)';
            const values = [name];
            const response = await conn.query(query, values);

            console.log(response);
            return res.status(200).json('creating status');     
            
        default:
            return res.status(400).json('invalid method');
    }

    

};
